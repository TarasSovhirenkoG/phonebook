﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using System.IO;
using Android.Content.Res;
using PhoneBook.Entity.Interfaces;

namespace PhoneBook.Droid
{
    class DataProvider : IDisposable
    {
        private string _dataBasePath {get; set; }
        private string _dataBaseName { get; set; }

        public DataProvider(string dbPath, string dbName)
        {
            this._dataBasePath = dbPath;
            this._dataBaseName = dbName;
        }

        public void CreateDatabase(Context context)
        {
            if (!File.Exists(_dataBasePath))
            {
                AssetManager assets = context.Assets;
                using (BinaryReader br = new BinaryReader(assets.Open(_dataBaseName)))
                {
                    using (BinaryWriter bw = new BinaryWriter(new FileStream(_dataBasePath, FileMode.Create)))
                    {
                        byte[] buffer = new byte[2048];
                        int len = 0;
                        while ((len = br.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            bw.Write(buffer, 0, len);
                        }
                    }
                }
                DropTable();
                CreateTable();
                FillTable();
            }
        }

        private void CreateTable()
        {
            try
            {
                var connection = new SQLiteConnection(_dataBasePath);
                connection.CreateTable<DBContactEntity>();
            }
            catch (SQLiteException ex)
            {
                
            }
        }

        private void DropTable()
        {
            try
            {
                var connection = new SQLiteConnection(_dataBasePath);
                connection.DropTable<DBContactEntity>();
            }
            catch (SQLiteException ex)
            {

            }
        }

        private void FillTable()
        {
            List<DBContactEntity> contacts = new List<DBContactEntity>()
            {
                new DBContactEntity { Name = "Taras", Surname = "Sovhirenko", MainPhoneNumber = "0955465158", Photo = "icon_contact_black" },
                new DBContactEntity { Name = "Mat", Surname = "Sovhirenko", MainPhoneNumber = "0523632369", Photo = "icon_contact_white" },
                new DBContactEntity { Name = "Ann", Surname = "Sovhirenko", MainPhoneNumber = "0955465158", Photo = "icon_contact_black"  },
                new DBContactEntity { Name = "Taras", Surname = "Sovhirenko", MainPhoneNumber = "0955465158", Photo ="icon_contact_white" },
                new DBContactEntity { Name = "Taras", Surname = "Sovhirenko", MainPhoneNumber = "0955465158", Photo = "icon_contact_black"  },
                new DBContactEntity { Name = "Taras", Surname = "Sovhirenko", MainPhoneNumber = "0955465158", Photo = "icon_contact_white" },
                new DBContactEntity { Name = "Taras", Surname = "Sovhirenko", MainPhoneNumber = "0955465158", Photo = "icon_contact_black"  },
                new DBContactEntity { Name = "Taras", Surname = "Sovhirenko", MainPhoneNumber = "0955465158", Photo ="icon_contact_white" },
                new DBContactEntity { Name = "Taras", Surname = "Sovhirenko", MainPhoneNumber = "0955465158", Photo ="icon_contact_black"  },
            };

            Insert(contacts);
        }

        public void Insert(DBContactEntity data)
        {
            try
            {
                var db = new SQLiteConnection(_dataBasePath);
                if (db.Insert(data) != 0)
                    db.Update(data);
            }
            catch (SQLiteException ex)
            {

            }
        }

        public void Insert(IEnumerable<DBContactEntity> data)
        {
            try
            {
                var db = new SQLiteConnection(_dataBasePath);
                if (db.InsertAll(data) != 0)
                    db.UpdateAll(data);
            }
            catch (SQLiteException ex)
            {

            }
        }

        public List<DBContactEntity> GetAllContacts()
        {
            List<DBContactEntity> allContacts = new List<DBContactEntity>();
            try
            {
                var db = new SQLiteConnection(_dataBasePath);
                var contacts = db.Table<DBContactEntity>();
                foreach (var row in contacts)
                {
                    allContacts.Add(row);
                }

                return allContacts;
            }
            catch (SQLiteException)
            {
                return allContacts;
            }
        }

        public void Dispose()
        {
            this.Dispose();
        }
    }
}