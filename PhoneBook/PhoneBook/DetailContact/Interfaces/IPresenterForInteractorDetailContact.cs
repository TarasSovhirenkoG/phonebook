﻿using PhoneBook.Entity;
using PhoneBook.Entity.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.DetailContact.Interfaces
{
    public interface IPresenterForInteractorDetailContact
    {
        void SetData(IDetailContactEntity entity);
        void AddPhone();
        void GoBack();
        //void ShowHideSpinner(bool isShow);

        void InvalidPhoneInput(int id);
    }
}
