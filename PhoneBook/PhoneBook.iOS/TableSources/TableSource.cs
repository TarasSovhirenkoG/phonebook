﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using PhoneBook.Entity.Interfaces;
using PhoneBook.iOS.Cells;

namespace PhoneBook.iOS.TableSources
{
    class TableSource : UITableViewSource
    {
        public event EventHandler<IDetailContactEntity> RowTouched;
        List<IDetailContactEntity> tableItems;
        private static readonly int _CustomRowHeight = 120;
        NSString cellIdentifier = new NSString("TableCell");

        public TableSource(List<IDetailContactEntity> items)
        {
            tableItems = items;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return tableItems.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            RowTouched(this, tableItems[indexPath.Row]);
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell cell;
            if (indexPath.Row % 2 == 0 )
            {
                cell = tableView.DequeueReusableCell(cellIdentifier) as ContactsCell;
                if (cell == null)
                {
                    cell = new ContactsCell(cellIdentifier);
                }
                ((ContactsCell)cell).UpdateCell(tableItems[indexPath.Row].Name, tableItems[indexPath.Row].Surname, tableItems[indexPath.Row].MainPhoneNumber, UIImage.FromFile("Images/" + tableItems[indexPath.Row].Photo));
                return cell;
            }
            else
            {
                cell = tableView.DequeueReusableCell(cellIdentifier) as ReverseContactCell;
                if (cell == null)
                {
                    cell = new ReverseContactCell(cellIdentifier);
                }
                ((ReverseContactCell)cell).UpdateCell(tableItems[indexPath.Row].Name, tableItems[indexPath.Row].Surname, tableItems[indexPath.Row].MainPhoneNumber, UIImage.FromFile("Images/" + tableItems[indexPath.Row].Photo));
                return cell;
            }
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return _CustomRowHeight;
        }
    }
}