﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.DetailContact.Interfaces
{
    public interface IRouterDetailContact
    {
        void GoBack();
    }
}
