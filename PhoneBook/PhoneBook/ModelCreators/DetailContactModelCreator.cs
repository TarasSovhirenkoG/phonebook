﻿using PhoneBook.Entity;
using PhoneBook.ModelCreators.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.ModelCreators
{
    public class DetailContactModelCreator : IDetailContactModelCreator
    {
        public DetailContactEntity GetModel()
        {
            DetailContactEntity model = new DetailContactEntity
            {
                Name = "Taras",
                Surname = "Sovhirenko",
                MainPhoneNumber = "380955465158"
            };

            return new DetailContactEntity(model.Name, model.Surname, model.MainPhoneNumber, model.AdditionalPhoneList);
        }
    }
}
