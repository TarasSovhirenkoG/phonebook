﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using CoreGraphics;

namespace PhoneBook.iOS.Cells
{
    class ReverseContactCell : UITableViewCell
    {
        private UILabel _labelName;
        private UILabel _labelSurname;
        private UILabel _labelPhone;
        private UIImageView _imagePhoto;

        public ReverseContactCell(NSString cellId) : base(UITableViewCellStyle.Default, cellId)
        {
            SelectionStyle = UITableViewCellSelectionStyle.Gray;

            _imagePhoto = new UIImageView();

            _labelName = new UILabel()
            {
                TextColor = UIColor.FromRGB(127, 51, 0),
                BackgroundColor = UIColor.Clear
            };

            _labelSurname = new UILabel()
            {
                TextColor = UIColor.FromRGB(127, 51, 0),
                BackgroundColor = UIColor.Clear
            };

            _labelPhone = new UILabel()
            {
                TextColor = UIColor.FromRGB(38, 127, 0),
                TextAlignment = UITextAlignment.Center,
                BackgroundColor = UIColor.Clear
            };

            ContentView.Add(_labelName);
            ContentView.Add(_labelSurname);
            ContentView.Add(_labelPhone);
            ContentView.Add(_imagePhoto);
        }

        public void UpdateCell(string name, string surname, string phone, UIImage image)
        {
            _imagePhoto.Image = image;
            _labelName.Text = name;
            _labelSurname.Text = surname;
            _labelPhone.Text = phone;
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            _imagePhoto.Frame = new CGRect(10, 5, ContentView.Bounds.Width / 3 - 10 , ContentView.Bounds.Width / 3 - 10);
            _labelName.Frame = new CGRect(ContentView.Bounds.Width - 2 * ContentView.Bounds.Width / 3, 10, ContentView.Bounds.Width - 2 * ContentView.Bounds.Width / 3 - 10, ContentView.Bounds.Height / 2 - 20);
            _labelSurname.Frame = new CGRect(ContentView.Bounds.Width - ContentView.Bounds.Width / 3, 10, ContentView.Bounds.Width - ContentView.Bounds.Width / 3 - 10, ContentView.Bounds.Height / 2 - 20);
            _labelPhone.Frame = new CGRect(ContentView.Bounds.Width - 2 * ContentView.Bounds.Width / 3 + 20, ContentView.Bounds.Height  - ContentView.Bounds.Height / 2 + 10, ContentView.Bounds.Width - ContentView.Bounds.Width / 3 - 20, ContentView.Bounds.Height / 2 - 20);
        }
    }
}