﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace PhoneBook.iOS.Views
{
    [Register ("DetailContactView")]
    partial class DetailContactView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _cancelButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField _nameTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField _phoneTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView _photoImageView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView _rootView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField _surnameTextField { get; set; }

        [Action ("CancelTouchUp:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void CancelTouchUp (UIKit.UIButton sender);

        [Action ("PhoneDidEndEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void PhoneDidEndEditing (UIKit.UITextField sender);

        void ReleaseDesignerOutlets ()
        {
            if (_cancelButton != null) {
                _cancelButton.Dispose ();
                _cancelButton = null;
            }

            if (_nameTextField != null) {
                _nameTextField.Dispose ();
                _nameTextField = null;
            }

            if (_phoneTextField != null) {
                _phoneTextField.Dispose ();
                _phoneTextField = null;
            }

            if (_photoImageView != null) {
                _photoImageView.Dispose ();
                _photoImageView = null;
            }

            if (_rootView != null) {
                _rootView.Dispose ();
                _rootView = null;
            }

            if (_surnameTextField != null) {
                _surnameTextField.Dispose ();
                _surnameTextField = null;
            }
        }
    }
}