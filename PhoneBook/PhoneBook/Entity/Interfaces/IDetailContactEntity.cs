﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Entity.Interfaces
{
    public interface IDetailContactEntity
    {
        string Name { get; set; }
        string Surname { get; set; }
        string MainPhoneNumber { get; set; }
        string Photo { get; set; }
        Dictionary<int, string> AdditionalPhoneList { get; set; }
    }
}
