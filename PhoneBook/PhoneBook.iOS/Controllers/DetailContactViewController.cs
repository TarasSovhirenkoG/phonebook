﻿using Foundation;
using PhoneBook.DetailContact.Implementation;
using PhoneBook.DetailContact.Interfaces;
using PhoneBook.ModelCreators;
using PhoneBook.Validation;
using System;
using UIKit;
using PhoneBook.Entity;
using PhoneBook.Entity.Interfaces;
using PhoneBook.iOS.Routers;

namespace PhoneBook.iOS.Controllers
{
    public partial class DetailContactViewController : UIViewController
    {
        public string SelectedKey { get; set; }
        public IDetailContactEntity Contact;
        private IPresenterDetailContact _presenter;

        public DetailContactViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var interactor = new InteractorDetailContact(new DetailContactModelCreator(), new ValidationDetailContact());
            interactor.SetData(Contact);

            _presenter = new PresenterDetailContact(_detailContactView, interactor, new RouterDetailContact(NavigationController));
            _presenter.SetConfig();

            NavigationController.NavigationBar.Hidden = true;
        }
    }
}