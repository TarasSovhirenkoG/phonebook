﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using PhoneBook.DetailContact.Interfaces;
using PhoneBook.DetailContact.Implementation;
using PhoneBook.Validation.Interfaces;
using PhoneBook.ModelCreators.Interfaces;
using PhoneBook.Entity.Interfaces;
using PhoneBook.Entity;
using System.Reflection;

namespace Tests
{
    [TestFixture]
    class PresenterDetailContactTest
    {
        private Mock<IInteractorDetailContact> _interactorMock;
        private Mock<IViewContactDeatil> _viewMock;
        private Mock<IRouterDetailContact> _routerMock;
        private IPresenterDetailContact _presenter;

        [SetUp]
        public void SetUp()
        {
            _interactorMock = new Mock<IInteractorDetailContact>(MockBehavior.Strict);
            _viewMock = new Mock<IViewContactDeatil>(MockBehavior.Strict);
            _routerMock = new Mock<IRouterDetailContact>(MockBehavior.Strict);
        }

        [Test]
        public void ConstructorTest()
        {
            _interactorMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForInteractorDetailContact>());
            _viewMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForViewDetailContact>());
            _presenter = new PresenterDetailContact(_viewMock.Object, _interactorMock.Object, _routerMock.Object);

            _interactorMock.VerifySet(f => f.Presenter = It.IsAny<IPresenterForInteractorDetailContact>(), Times.Once);
            _viewMock.VerifySet(f => f.Presenter = It.IsAny<IPresenterForViewDetailContact>(), Times.Once);
        }

        [Test]
        public void CancelClickTest()
        {
            _interactorMock.Setup(f => f.CancelClick());
            _interactorMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForInteractorDetailContact>());
            _viewMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForViewDetailContact>());
            _presenter = new PresenterDetailContact(_viewMock.Object, _interactorMock.Object, _routerMock.Object);
            _presenter.CancelClick();

            _interactorMock.Verify(f => f.CancelClick(), Times.Once);
        }

        [Test]
        public void GoBackTest()
        {
            _routerMock.Setup(f => f.GoBack());
            _interactorMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForInteractorDetailContact>());
            _viewMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForViewDetailContact>());
            _presenter = new PresenterDetailContact(_viewMock.Object, _interactorMock.Object, _routerMock.Object);
            _presenter.GoBack();

            _routerMock.Verify(f => f.GoBack(), Times.Once);
        }

        [Test]
        public void InvalidPhoneInputTest()
        {
            _viewMock.Setup(f => f.InvalidPhone(It.IsAny<int>()));
            _interactorMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForInteractorDetailContact>());
            _viewMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForViewDetailContact>());
            _presenter = new PresenterDetailContact(_viewMock.Object, _interactorMock.Object, _routerMock.Object);
            _presenter.InvalidPhoneInput(0);

            _viewMock.Verify(f => f.InvalidPhone(It.IsAny<int>()), Times.Once);
        }

        [Test]
        public void SetDataTest()
        {
            _viewMock.Setup(f => f.SetName(null));
            _viewMock.Setup(f => f.SetSurname(null));
            _viewMock.Setup(f => f.SetPhoneNumber(null));
            _viewMock.Setup(f => f.SetPhoto(null));
            _interactorMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForInteractorDetailContact>());
            _viewMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForViewDetailContact>());
            _presenter = new PresenterDetailContact(_viewMock.Object, _interactorMock.Object, _routerMock.Object);
            _presenter.SetData(new DetailContactEntity(null, null, null, null));

            _viewMock.Verify(f => f.SetName(null), Times.Once);
            _viewMock.Verify(f => f.SetSurname(null), Times.Once);
            _viewMock.Verify(f => f.SetPhoneNumber(null), Times.Once);
            _viewMock.Verify(f => f.SetPhoto(null), Times.Once);
        }

        [Test]
        public void AddPhoneTest()
        {
            _viewMock.Setup(f => f.AddPhone(It.IsAny<string>()));

            _interactorMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForInteractorDetailContact>());
            _viewMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForViewDetailContact>());

            _presenter = new PresenterDetailContact(_viewMock.Object, _interactorMock.Object, _routerMock.Object);
            _presenter.AddPhone();

            _viewMock.Verify(f => f.AddPhone(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void AddClickTest()
        {
            _interactorMock.Setup(f => f.AddClick());

            _interactorMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForInteractorDetailContact>());
            _viewMock.SetupSet(f => f.Presenter = It.IsAny<IPresenterForViewDetailContact>());

            _presenter = new PresenterDetailContact(_viewMock.Object, _interactorMock.Object, _routerMock.Object);
            _presenter.AddClick();

            _interactorMock.Verify(f => f.AddClick(), Times.Once);
        }
    }
}