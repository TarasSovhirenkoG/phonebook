﻿using PhoneBook.DetailContact.Interfaces;
using PhoneBook.Entity;
using PhoneBook.Entity.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.DetailContact.Implementation
{
    public class PresenterDetailContact : IPresenterDetailContact
    {
        private readonly IViewContactDeatil _view;
        private readonly IRouterDetailContact _router;
        private readonly IInteractorDetailContact _interactor;

        public PresenterDetailContact(IViewContactDeatil view, IInteractorDetailContact interactor, IRouterDetailContact router)
        {
            _view = view ?? throw new NullReferenceException(nameof(_view));
            _interactor = interactor ?? throw new NullReferenceException(nameof(_interactor));
            _router = router ?? throw new NullReferenceException(nameof(_router));
            _interactor.Presenter = this;
            _view.Presenter = this;
        }

        public void PhoneTextChanged(int id, string text)
        {
            var valid = _interactor.PhoneTextChanged(id, text);

            if (valid)
            {
                _view.ValidPhone(id);
            }
            else
            {
                _view.InvalidPhone(id);
            }
        }

        public void InvalidPhoneInput(int id)
        {
            _view.InvalidPhone(id);
        }

        public void SaveClick(string name, string surname, string phone, Dictionary<int, string> phoneList)
        {
            _interactor.SaveClick(name, surname, phone, phoneList);
        }

        public void CancelClick()
        {
            _interactor.CancelClick();
        }

        public void AddClick()
        {
            _interactor.AddClick();
        }

        public void SetConfig()
        {
            _interactor.SetConfig();
        }


        public void SetData(IDetailContactEntity entity)
        {
            _view.SetName(entity.Name);
            _view.SetSurname(entity.Surname);
            _view.SetPhoneNumber(entity.MainPhoneNumber);
            _view.SetPhoto(entity.Photo);
        }

        public void GoBack()
        {
            _router.GoBack();
        }

        public void AddPhone()
        {
            _view.AddPhone();
        }
    }
}
