﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using PhoneBook.DetailContact.Interfaces;
using UIKit;

namespace PhoneBook.iOS.Routers
{
    class RouterDetailContact : IRouterDetailContact
    {
        UINavigationController _navigationController;

        public RouterDetailContact(UINavigationController navigationController)
        {
            _navigationController = navigationController;
        }

        public void GoBack()
        {
            _navigationController.PopViewController(true);
        }
    }
}