﻿using PhoneBook.DetailContact.Interfaces;
using PhoneBook.Entity.Interfaces;
using PhoneBook.Entity;
using PhoneBook.ModelCreators.Interfaces;
using PhoneBook.Validation.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.DetailContact.Implementation
{
    public class InteractorDetailContact : IInteractorDetailContact
    {
        private readonly IValidationDetailContact _validation;
        private readonly IDetailContactModelCreator _modelCreator;
        private IPresenterForInteractorDetailContact _presenter;

        private IDetailContactEntity _contact;

        public IPresenterForInteractorDetailContact Presenter
        {
            private get
            {
                return _presenter;
            }
            set
            {
                _presenter = value ?? throw new ArgumentNullException(nameof(value));
            }
        }

        public InteractorDetailContact(IDetailContactModelCreator modelCreator, IValidationDetailContact validation)
        {
            _validation = validation ?? throw new ArgumentNullException(nameof(validation));
            _modelCreator = modelCreator ?? throw new ArgumentNullException(nameof(modelCreator));
        }

        public void CancelClick()
        {
            Presenter.GoBack();
        }

        public void SaveClick(string name, string surname, string phone, Dictionary<int, string> phoneList)
        {
            var entity = new DetailContactEntity(name, surname, phone, phoneList);

            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            foreach(var pair in entity.AdditionalPhoneList)
            {
                if (!PhoneTextChanged(pair.Key, pair.Value))
                {
                    Presenter.InvalidPhoneInput(pair.Key);
                }
            }
        }

        public bool PhoneTextChanged(int id, string phone)
        {
            if (phone == null)
            {
                throw new ArgumentNullException(nameof(phone));
            }
            return _validation.CheckPhone(phone);
        }

        public void SetData(IDetailContactEntity contact)
        {
            _contact = contact;
        }

        public void SetConfig()
        {
            Presenter.SetData(_contact);
        }


        public void Dispose()
        {
        }

        public void AddClick()
        {
            Presenter.AddPhone();
        }
    }
}
