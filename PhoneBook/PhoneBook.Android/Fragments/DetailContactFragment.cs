﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using PhoneBook;
using PhoneBook.DetailContact.Implementation;
using PhoneBook.DetailContact.Interfaces;
using PhoneBook.Droid.Routers.DetailContact;
using PhoneBook.Droid.Views.DetailContact;
using PhoneBook.ModelCreators;
using PhoneBook.Validation;
using PhoneBook.Entity;
using PhoneBook.Entity.Interfaces;

namespace PhoneBook.Droid.Fragments
{
    public class DetailContactFragment : Fragment
    {
        public string SelectedKey { get; set; }
        private IPresenterDetailContact _presenter;
        private IDetailContactEntity _contact;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.DetailContactFragment, container, false);

            if (Arguments != null)
            {
                _contact = new DetailContactEntity()
                {
                    Name = Arguments.GetString("Name"),
                    Surname = Arguments.GetString("SurName"),
                    MainPhoneNumber = Arguments.GetString("Phone"),
                    Photo = Arguments.GetString("Photo"),
                    AdditionalPhoneList = new Dictionary<int,string>()
                };
            }

            var editContactView = view.FindViewById<ContactDetailView>(Resource.Id.detailContact_View);

            Holder holder = new Holder(view);

            var interactor = new InteractorDetailContact(new DetailContactModelCreator(), new ValidationDetailContact());
            interactor.SetData(_contact);

            _presenter = new PresenterDetailContact(editContactView, interactor, new RouterDetailContact(this));
            _presenter.SetConfig();

            return view;
        }

        private class Holder
        {
            public IViewContactDeatil DetailContactView { get; private set; }

            public Holder(View view)
            {
                DetailContactView = view.FindViewById<ContactDetailView>(Resource.Id.detailContact_View);
            }
        }
    }
}