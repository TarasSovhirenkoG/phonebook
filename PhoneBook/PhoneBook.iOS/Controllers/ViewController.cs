﻿using System;

using UIKit;
using CoreGraphics;
using System.Collections.Generic;
using PhoneBook.Entity.Interfaces;
using PhoneBook.Entity;
using PhoneBook.iOS.TableSources;

namespace PhoneBook.iOS.Controllers
{
    public partial class ViewController : UIViewController
    {
        private UITableView table;

        int count = 1;

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            table = new UITableView(View.Bounds);
            table.AutoresizingMask = UIViewAutoresizing.All;

            table.SeparatorColor = UIColor.FromRGB(127, 106, 0);

            List<IDetailContactEntity> tableItems = new List<IDetailContactEntity>
            {
                new DetailContactEntity() { Name = "Taras", Surname = "Sovhirenko", MainPhoneNumber = "380955465158", Photo = "icon_contact_black.png" },
                new DetailContactEntity() { Name = "Mat", Surname = "Sovhirenko", MainPhoneNumber = "380955465158", Photo = "icon_contact_black.png" },
                new DetailContactEntity() { Name = "Anna", Surname = "Sovhirenko", MainPhoneNumber = "380955465158", Photo = "icon_contact_black.png" },
                new DetailContactEntity() { Name = "Anastasiya", Surname = "Sovhirenko", MainPhoneNumber = "380955465158", Photo = "icon_contact_black.png" },
                new DetailContactEntity() { Name = "Taras", Surname = "Sovhirenko", MainPhoneNumber = "380955465158", Photo = "icon_contact_black.png" },
                new DetailContactEntity() { Name = "Mat", Surname = "Sovhirenko", MainPhoneNumber = "380955465158", Photo = "icon_contact_black.png" }
            };

            var source = new TableSource(tableItems);
            
            source.RowTouched += (sender, item) => 
            {
                var detailContactViewController = (DetailContactViewController)UIStoryboard.FromName("Main", null).InstantiateViewController("DetailContact");
                detailContactViewController.Contact = item;
                NavigationController.PushViewController(detailContactViewController, true);
            };

            table.Source = source;
            Add(table);
        }
    }
}
