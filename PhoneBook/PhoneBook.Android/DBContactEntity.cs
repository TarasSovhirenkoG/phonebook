﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using Newtonsoft.Json;
using PhoneBook.Entity.Interfaces;

namespace PhoneBook.Droid
{
    class DBContactEntity : IDetailContactEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string MainPhoneNumber { get; set; }
        public string Photo { get; set; }
        [Ignore]
        public Dictionary<int, string> AdditionalPhoneList { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

    }
}