﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using PhoneBook;
using PhoneBook.DetailContact.Interfaces;
using Android.Support.V7.Widget;
using PhoneBook.Droid.Adapters;

namespace PhoneBook.Droid.Views.DetailContact
{
    [Register("com.companyname.PhoneBook.Android.ContactDetailView")]
    public class ContactDetailView : RelativeLayout, IViewContactDeatil
    {
        private Holder _holder;
        private LayoutInflater _inflater;

        private bool _phoneChanged;

        public IPresenterForViewDetailContact Presenter { private get; set; }


        public ContactDetailView(Context context) : base(context)
        {
            Init(context);
        }

        public ContactDetailView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Init(context);
        }

        public ContactDetailView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs,
            defStyleAttr)
        {
            Init(context);
        }

        public ContactDetailView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context,
            attrs, defStyleAttr, defStyleRes)
        {
            Init(context);
        }

        public void AddPhone(string phone)
        {
            EditText phoneEditText = new EditText(Context);
            phoneEditText.Id = View.GenerateViewId();

            (Context as Activity)?.RunOnUiThread(() =>
            {
                phoneEditText.Gravity = GravityFlags.Center;
                phoneEditText.Text = phone;

                _holder.ContainerLinearLayout.AddView(phoneEditText);
            });

            phoneEditText.AfterTextChanged += (s, e) =>
            {
                _phoneChanged = true;
            };

            phoneEditText.FocusChange += (s, e) =>
            {
                if (_phoneChanged)
                {
                    _phoneChanged = false;
                    Presenter.PhoneTextChanged(phoneEditText.Id, phoneEditText.Text);
                }
            };
        }

        private void Init(Context context)
        {
            _inflater = ((Activity)context).LayoutInflater;
            _inflater.Inflate(Resource.Layout.ContactDetailView, this, true);
            _holder = new Holder(this);

            SetButtonsActions();
        }

        private void SetButtonsActions()
        {
            if (!_holder.SaveButton.HasOnClickListeners)
            {
                _holder.SaveButton.Click += (s, e) =>
                {
                    var phoneList = new Dictionary<int, string>();
                    int childCount = _holder.ContainerLinearLayout.ChildCount;
                    for (int i = 0; i < childCount; i++)
                    {
                        var editPhone = ((EditText)_holder.ContainerLinearLayout.GetChildAt(i));
                        phoneList.Add(editPhone.Id, editPhone.Text);
                    }

                    Presenter.SaveClick(_holder.NameEditText.Text, _holder.LastnameEditText.Text, "", phoneList);
                };
            }

            if (!_holder.AddButton.HasOnClickListeners)
            {
                _holder.AddButton.Click += (s, e) =>
                {
                    Presenter.AddClick();
                };
            }

            if (!_holder.CancelButton.HasOnClickListeners)
            {
                _holder.CancelButton.Click += (s, e) =>
                {
                    Presenter.CancelClick();
                };
            }
        }


        public void SetName(string text)
        {
            (Context as Activity)?.RunOnUiThread(() =>
            {
                _holder.NameEditText.Text = text;
            });
        }

        public void SetSurname(string text)
        {
            (Context as Activity)?.RunOnUiThread(() =>
            {
                _holder.LastnameEditText.Text = text;
            });
        }

        public void SetPhoneNumber(string text)
        {
            AddPhone(text);
        }

        public void SetPhoto(string photo)
        {
            (Context as Activity)?.RunOnUiThread(() =>
            {
                _holder.PhotoImageView.SetImageResource((int)typeof(Resource.Drawable).GetField(photo).GetValue(null));
            });
        }

        public void InvalidPhone(int id)
        {
            (Context as Activity)?.RunOnUiThread(() =>
            {
                EditText invalidPhone = (EditText)_holder.ContainerLinearLayout.FindViewById<EditText>(id);
                invalidPhone.SetTextColor(Android.Graphics.Color.Red);
            });

        }

        public void ValidPhone(int id)
        {
            (Context as Activity)?.RunOnUiThread(() =>
            {
                EditText invalidPhone = (EditText)_holder.ContainerLinearLayout.FindViewById<EditText>(id);
                invalidPhone.SetTextColor(Android.Graphics.Color.White);
            });
        }

        private class Holder
        {
            public Holder(View viewGroup)
            {
                NameEditText = viewGroup.FindViewById<EditText>(Resource.Id.detailContact_name_editText);
                LastnameEditText = viewGroup.FindViewById<EditText>(Resource.Id.detailContact_surname_editText);
                //PhoneEditText = viewGroup.FindViewById<EditText>(Resource.Id.editPhone);

                AddButton = viewGroup.FindViewById<Button>(Resource.Id.detailContact_add_button);
                SaveButton = viewGroup.FindViewById<Button>(Resource.Id.detailContact_save_button);
                CancelButton = viewGroup.FindViewById<Button>(Resource.Id.detailContact_cancel_button);

                PhotoImageView = viewGroup.FindViewById<ImageView>(Resource.Id.imgView);

                ContainerLinearLayout = viewGroup.FindViewById<LinearLayout>(Resource.Id.detailContact_card_linearLayout);
            }

            public EditText NameEditText { get; }
            public EditText LastnameEditText { get; }
            //public EditText PhoneEditText { get; }

            public ImageView PhotoImageView { get; }

            public LinearLayout ContainerLinearLayout { get; }

            public Button AddButton { get; }
            public Button SaveButton { get; }
            public Button CancelButton { get; }
        }
    }
}