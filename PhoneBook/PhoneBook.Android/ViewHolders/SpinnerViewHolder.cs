﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace PhoneBook.Droid.ViewHolders
{
    class SpinnerViewHolder : RecyclerView.ViewHolder
    {
        public ProgressBar Spinner { get; set; }

        public SpinnerViewHolder(View view) : base(view)
        {
            Spinner = view.FindViewById<ProgressBar>(Resource.Id.pBar);
        }
    }
}