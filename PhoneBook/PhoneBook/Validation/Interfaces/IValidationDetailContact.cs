﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Validation.Interfaces
{
    public interface IValidationDetailContact
    {
        bool CheckPhone(string phone);
    }
}
