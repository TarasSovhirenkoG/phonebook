﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace PhoneBook.iOS.Controllers
{
    [Register ("DetailContactController")]
    partial class DetailContactViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        PhoneBook.iOS.Views.DetailContactView _detailContactView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_detailContactView != null) {
                _detailContactView.Dispose ();
                _detailContactView = null;
            }
        }
    }
}