﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace PhoneBook.Droid
{
    [Activity(Label = "Contact Details", Icon = "@mipmap/icon")]
    public class ContactProfileActivity : Activity
    {
        private List<string> _phones;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ContactProfileActivity);

            var contact = JsonConvert.DeserializeObject<DBContactEntity>(Intent.GetStringExtra("Contact"));

            TextView TextViewName = FindViewById<TextView>(Resource.Id.detailContact_name_textView);
            TextView TextViewSurname = FindViewById<TextView>(Resource.Id.detailContact_surname_textView);
            ImageView ImageViewPhoto = FindViewById<ImageView>(Resource.Id.imgView);

            TextViewName.Text = contact.Name;
            TextViewSurname.Text = contact.Surname;
            //ImageViewPhoto.SetImageResource(contact.Photo);

            Button button = FindViewById<Button>(Resource.Id.detailContact_add_button);
            button.Click += AddPhone_Click;

            _phones = new List<string>();
            var listView = FindViewById<ListView>(Resource.Id.listView);
            //listView.Adapter = new ArrayAdapter<string>(this, Resource.Layout.PhoneCell, Resource.Id.phoneNumber);
            ArrayAdapter<string> adapter = (ArrayAdapter<string>)FindViewById<ListView>(Resource.Id.listView).Adapter;
            adapter.Add(contact.MainPhoneNumber);
        }

        private void AddPhone_Click(object sender, EventArgs e)
        {
            ArrayAdapter<string> adapter = (ArrayAdapter<string>)FindViewById<ListView>(Resource.Id.listView).Adapter;
            var listView = FindViewById<ListView>(Resource.Id.listView);
            
            adapter.Add(FindViewById<TextView>(Resource.Id.editPhone).Text);
        }
    }
}