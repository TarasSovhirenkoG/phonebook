﻿ using Foundation;
using System;
using UIKit;
using PhoneBook.DetailContact.Interfaces;
using System.Collections.Generic;

namespace PhoneBook.iOS.Views
{
    public partial class DetailContactView : UIView, IViewContactDeatil
    {
        public DetailContactView (IntPtr handle) : base (handle)
        {
        }

        public IPresenterForViewDetailContact Presenter { private get; set; }

        public Func<UIView, UIViewController> GetViewController { get; set; }
        private List<UITextField> _listPhones { get; set; }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            NSBundle.MainBundle.LoadNib("DetailContactView", this, null);

            InvokeOnMainThread(() =>
            {
                var frame = _rootView.Frame;
                frame.Height = Frame.Height;
                frame.Width = Frame.Width;
                _rootView.Frame = frame;
                AddSubview(_rootView);
            });
            _listPhones = new List<UITextField>();
            SetActions();
        }

        private void SetActions()
        {
            _nameTextField.ShouldBeginEditing = (sender) => false;
            _surnameTextField.ShouldBeginEditing = (sender) => false;

            _phoneTextField.KeyboardType = UIKeyboardType.PhonePad;
            HideShowKeyboard();
        }

        private bool ResignResponder(UITextField textField)
        {
            textField.ResignFirstResponder();
            return true;
        }

        private void HideShowKeyboard()
        {
            _nameTextField.ShouldReturn = ResignResponder;
            _surnameTextField.ShouldReturn = ResignResponder;
            _phoneTextField.ShouldReturn = ResignResponder;

            UITapGestureRecognizer tapGestureRecognizer = new UITapGestureRecognizer(() =>
            {
                _nameTextField.ResignFirstResponder();
                _surnameTextField.ResignFirstResponder();
                _phoneTextField.ResignFirstResponder();
            });

            AddGestureRecognizer(tapGestureRecognizer);
        }

        partial void CancelTouchUp(UIButton sender)
        {
            Presenter.AddClick();
        }

        partial void PhoneDidEndEditing(UITextField sender)
        {
            Presenter.PhoneTextChanged(0, _phoneTextField.Text);
        }

        public void SetCancelButtonLocale(string text)
        {
            InvokeOnMainThread(() =>
            {
                _cancelButton.SetTitle(text, UIControlState.Normal);
            });
        }

        public void SetName(string text)
        {
            InvokeOnMainThread(() =>
            {
                _nameTextField.Text = text;
            });
        }

        public void SetSurname(string text)
        {
            InvokeOnMainThread(() =>
            {
                _surnameTextField.Text = text;
            });
        }

        public void SetPhoto(string photo)
        {
            InvokeOnMainThread(() =>
            {
                _photoImageView.Image = UIImage.FromFile("Images/" + photo);
            });
        }

        public void SetPhoneNumber(string text)
        {
            InvokeOnMainThread(() =>
            {
                AddPhone(text);
            });
        }

        public void AddPhone(string phone = "")
        {
            var phoneField = new UITextField()
            {
                Text = phone
            };

            phoneField.AccessibilityIdentifier = _listPhones.Count.ToString();
            phoneField.BorderStyle = UITextBorderStyle.RoundedRect;
            phoneField.KeyboardType = UIKeyboardType.PhonePad;

            phoneField.EditingDidEnd += (object sender, EventArgs e) =>
            {
                Presenter.PhoneTextChanged(Convert.ToInt32(phoneField.AccessibilityIdentifier), phoneField.Text);
            };

            AddSubview(phoneField);

            phoneField.TranslatesAutoresizingMaskIntoConstraints = false;
            phoneField.LeftAnchor.ConstraintEqualTo(_photoImageView.LeftAnchor, 0).Active = true;

            if (_listPhones.Count == 0)
            {
                phoneField.TopAnchor.ConstraintEqualTo(_photoImageView.BottomAnchor, 10).Active = true;
            }
            else
            {
                phoneField.TopAnchor.ConstraintEqualTo(_listPhones[_listPhones.Count - 1].BottomAnchor, 10).Active = true;
            }
            _listPhones.Add(phoneField);

            phoneField.HeightAnchor.ConstraintEqualTo(30).Active = true;
            phoneField.WidthAnchor.ConstraintEqualTo(200).Active = true;

        }

        public void InvalidPhone(int id)
        {
            _listPhones[id].TextColor = UIColor.Red;
        }

        public void ValidPhone(int id)
        {
            _listPhones[id].TextColor = UIColor.Black;
        }
    }
}