﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.DetailContact.Interfaces
{
    public interface IPresenterForViewDetailContact
    {
        void PhoneTextChanged(int id, string text);

        void SaveClick(string name, string surname, string phone, Dictionary<int, string> phoneList);
        void AddClick();
        void CancelClick();
    }
}
