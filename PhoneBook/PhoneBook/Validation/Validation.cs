﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace PhoneBook.Validation
{
    public static class Validation
    {
        public static bool CheckValue(string regexString, string valueToCheck)
        {
            if (valueToCheck == null || regexString == null)
            {
                return false;
            }

            Regex regex = new Regex(regexString);
            return regex.IsMatch(valueToCheck);
        }
    }
}
