﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using PhoneBook.Droid.Adapters;
using PhoneBook.Entity.Interfaces;

namespace PhoneBook.Droid.Fragments
{
    public class ContactListFragment : Fragment
    {
        private View _view;

        private RecyclerView _recyclerView;
        private RecyclerView.LayoutManager _recyclerViewLayoutManger;
        private RecyclerView.Adapter _recyclerViewAdapter;
        private List<IDetailContactEntity> _contactItems;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            _view = inflater.Inflate(Resource.Layout.ContactListFragment, null, false);
            Context context = this.Activity;

            _recyclerView = _view.FindViewById<RecyclerView>(Resource.Id.recyclerView);

            var dbName = "db_contacts.sqlite";
            var dbPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments), dbName);

            DataProvider provider = new DataProvider(dbPath, dbName);
            provider.CreateDatabase(context);
            var dbContacts = provider.GetAllContacts();

            _contactItems = new List<IDetailContactEntity>();
            foreach (var contact in dbContacts)
            {
                _contactItems.Add(contact);
            }

            _recyclerViewLayoutManger = new LinearLayoutManager(context, LinearLayoutManager.Vertical, false);
            _recyclerView.SetLayoutManager(_recyclerViewLayoutManger);
            _recyclerViewAdapter = new ListContactRecyclerAdapter(_contactItems, context);
            _recyclerView.SetAdapter(_recyclerViewAdapter);

            return _view;
        }
    }
}