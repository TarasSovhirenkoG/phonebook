﻿using PhoneBook.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.ModelCreators.Interfaces
{
    public interface IDetailContactModelCreator
    {
        DetailContactEntity GetModel();
    }
}
