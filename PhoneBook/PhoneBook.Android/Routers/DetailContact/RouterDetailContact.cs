﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using PhoneBook;
using PhoneBook.DetailContact.Interfaces;
using PhoneBook.Droid.Fragments;

namespace PhoneBook.Droid.Routers.DetailContact
{
    internal class RouterDetailContact : IRouterDetailContact
    {
        /*private Context _context;

        public void OnBack()
        {
            (_context as Activity).Finish();
        }

        public void ToContactDetail(string id)
        {
            Intent intent = new Intent(_context, typeof(ContactProfileActivity));
            intent.PutExtra("id", id);
            _context.StartActivity(intent);
        }*/

        private readonly DetailContactFragment _fragment;

        public RouterDetailContact(DetailContactFragment fragment)
        {
            _fragment = fragment ?? throw new NullReferenceException(nameof(fragment));
        }

        public void GoBack()
        {
            var manager = (InputMethodManager)_fragment.Activity.GetSystemService(Activity.InputMethodService);
            manager.HideSoftInputFromWindow(_fragment.Activity.CurrentFocus?.WindowToken, 0);

            _fragment.FragmentManager.PopBackStack();
        }
    }
}