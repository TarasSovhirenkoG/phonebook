﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using PhoneBook.Droid.ViewHolders;
using PhoneBook.Droid.Fragments;
using PhoneBook.Enumerations;
using PhoneBook.Entity.Interfaces;

namespace PhoneBook.Droid.Adapters
{
    class ListContactRecyclerAdapter : RecyclerView.Adapter, IItemClickListener
    {
        private List<IDetailContactEntity> _contacts;
        private Context _context;

        public ListContactRecyclerAdapter(List<IDetailContactEntity> contacts, Context context)
        {
            contacts.Insert(0, null);
            contacts.Add(null);
            this._contacts = contacts;
            this._context = context;
            NotifyDataSetChanged();
        }

        public override int GetItemViewType(int position)
        {
            if (position == 0 || position == _contacts.Count - 1)
                return (int)ECellType.Spinner;
            if (position % 2 == 0)
                return (int)ECellType.PhotoDesription;
            return (int)ECellType.DescriptionPhoto;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View view;
            
            switch ((ECellType)viewType)
            {
                case ECellType.PhotoDesription:
                    view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.ReverseContactCell, parent, false);
                    view.LayoutParameters.Height = parent.Height / 5;
                    return new ReverseContactViewHolder(view);
                case ECellType.DescriptionPhoto:
                    view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.ContactCell, parent, false);
                    view.LayoutParameters.Height = parent.Height / 5;
                    return new ContactViewHolder(view);
                case ECellType.Spinner:
                    view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.SpinnerCell, parent, false);
                    view.LayoutParameters.Height = parent.Height / 5;
                    return new SpinnerViewHolder(view);
            }

            return null;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            if (holder is ContactViewHolder)
            {
                ((ContactViewHolder)holder).TextViewName.Text = _contacts[position].Name;
                ((ContactViewHolder)holder).TextViewSurname.Text = _contacts[position].Surname;
                ((ContactViewHolder)holder).TextViewNumber.Text = _contacts[position].MainPhoneNumber;
                ((ContactViewHolder)holder).ImageViewPhoto.SetImageResource((int)typeof(Resource.Drawable).GetField(_contacts[position].Photo).GetValue(null));
                ((ContactViewHolder)holder).SetItemClickListener(this);
            }
            else if (holder is ReverseContactViewHolder)
            {
                ((ReverseContactViewHolder)holder).TextViewName.Text = _contacts[position].Name;
                ((ReverseContactViewHolder)holder).TextViewSurname.Text = _contacts[position].Surname;
                ((ReverseContactViewHolder)holder).TextViewNumber.Text = _contacts[position].MainPhoneNumber;
                ((ReverseContactViewHolder)holder).ImageViewPhoto.SetImageResource((int)typeof(Resource.Drawable).GetField(_contacts[position].Photo).GetValue(null));
                ((ReverseContactViewHolder)holder).SetItemClickListener(this);
            }
        }

        public void OnClick(View view, int position)
        {
            DetailContactFragment fragment = new DetailContactFragment();
            Bundle args = new Bundle();
            args.PutString("Name", _contacts[position].Name);
            args.PutString("SurName", _contacts[position].Surname);
            args.PutString("Phone", _contacts[position].MainPhoneNumber);
            args.PutString("Photo", _contacts[position].Photo);
            fragment.Arguments = args;

            var fmTransaction = ((Activity)_context).FragmentManager.BeginTransaction();
            fmTransaction.Replace(Resource.Id.contacts_fragment_container, fragment, nameof(DetailContactFragment));
            fmTransaction.AddToBackStack(null);
            fmTransaction.Commit();
        }

        public override int ItemCount
        {
            get
            {
                return _contacts.Count;
            }
        }
    }
}