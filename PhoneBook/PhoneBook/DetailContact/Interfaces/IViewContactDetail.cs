﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.DetailContact.Interfaces
{
    public interface IViewContactDeatil
    {
        IPresenterForViewDetailContact Presenter { set; }

        void SetName(string text);
        void SetSurname(string text);
        void SetPhoneNumber(string text);
        void SetPhoto(string photo);
        void AddPhone(string phone = "");
        void InvalidPhone(int id);
        void ValidPhone(int id);
    }
}
