﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using PhoneBook.DetailContact.Interfaces;
using PhoneBook.DetailContact.Implementation;
using PhoneBook.Validation.Interfaces;
using PhoneBook.ModelCreators.Interfaces;
using PhoneBook.Entity.Interfaces;
using System.Reflection;

namespace Tests
{
    [TestFixture ]
    public class InteractorDetailContactTests
    {
        private Mock<IPresenterForInteractorDetailContact> _presenterMock;
        private Mock<IDetailContactModelCreator> _modelCreator;
        private Mock<IValidationDetailContact> _validation;
        private IInteractorDetailContact _interactor;

        [SetUp]
        public void SetUp()
        {
            _validation = new Mock<IValidationDetailContact>(MockBehavior.Strict);
            _modelCreator = new Mock<IDetailContactModelCreator>(MockBehavior.Strict);
            _interactor = new InteractorDetailContact(_modelCreator.Object, _validation.Object);
            _presenterMock = new Mock<IPresenterForInteractorDetailContact>(MockBehavior.Strict);

            _interactor.Presenter = _presenterMock.Object;
        }

        [Test]
        public void ConstructorTest()
        {
            Assert.IsInstanceOf<IInteractorDetailContact>(_interactor);
        }

        [Test]
        public void ConstructorNegativeTests()
        {
            Assert.Throws<ArgumentNullException>
        }

        [Test]
        public void SetPresenterForInteractorTest()
        {
            var presenter = _interactor.GetType().GetRuntimeProperties().First(f => f.Name.Equals("Presenter")).GetValue(_interactor);
            var result = _presenterMock.Object.Equals(presenter);
            Assert.IsTrue(result);
        }

        [Test]
        public void CancelClickTest()
        {
            _presenterMock.Setup(f => f.GoBack());
            _interactor.CancelClick();
            _presenterMock.Verify(f => f.GoBack(), Times.Once);
        }

        [Test]
        public void AddClickTest()
        {
            _presenterMock.Setup(f => f.AddPhone());
            _interactor.AddClick();
            _presenterMock.Verify(f => f.AddPhone(), Times.Once);
        }

        [Test]
        public void SetConfigTest()
        {
            _presenterMock.Setup(f => f.SetData(It.IsAny<IDetailContactEntity>()));
            _interactor.SetConfig();
            _presenterMock.Verify(f => f.SetData(It.IsAny<IDetailContactEntity>()), Times.Once);
        }

        [Test]
        public void PhoneTextChangedTest()
        {
            _validation.Setup(f => f.CheckPhone(It.IsAny<string>())).Returns(It.IsAny<bool>());
            _interactor.PhoneTextChanged(0, "");
            _validation.Verify(f => f.CheckPhone(It.IsAny<string>()), Times.Once);
        }
    }
}
