﻿using NUnit.Framework;
using PhoneBook.Validation;
using PhoneBook.Validation.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    class ValidationPhoneTests
    {
        private readonly IValidationDetailContact _validationEditContact = new ValidationDetailContact();

        [TestCase("123456", true)]
        [TestCase("123456123456123456", true)]
        [TestCase(null, true)]
        [TestCase("", true)]
        [TestCase("1234561234561234561", false)]
        [TestCase("12345", false)]
        [TestCase("qwertyQQWWEERRrty", false)]
        [TestCase("12345qwertyQQWWEERRrty", false)]
        [TestCase(".,-()\\/'+:=?!\"%&*;<>$", false)]
        [TestCase("1323 2134", false)]

        public void TestPhone(string phoneNumber, bool isValid)
        {
            Assert.AreEqual(isValid, _validationEditContact.CheckPhone(phoneNumber));
        }
    }
}
