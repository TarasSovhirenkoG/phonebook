package md51979bf339853ddcf2accd687d9d2f7f7;


public class PhoneViewHolder
	extends android.support.v7.widget.RecyclerView.ViewHolder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("PhoneBook.Droid.Adapters.PhoneViewHolder, PhoneBook.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", PhoneViewHolder.class, __md_methods);
	}


	public PhoneViewHolder (android.view.View p0)
	{
		super (p0);
		if (getClass () == PhoneViewHolder.class)
			mono.android.TypeManager.Activate ("PhoneBook.Droid.Adapters.PhoneViewHolder, PhoneBook.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Views.View, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
