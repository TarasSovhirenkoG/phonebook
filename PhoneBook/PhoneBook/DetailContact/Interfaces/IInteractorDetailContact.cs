﻿using PhoneBook.Entity.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.DetailContact.Interfaces
{
    public interface IInteractorDetailContact : IDisposable
    {
        IPresenterForInteractorDetailContact Presenter { set; }

        void SaveClick(string name, string surname, string phone, Dictionary<int, string> phoneList);
        void AddClick();
        void CancelClick();
        void SetConfig();

        bool PhoneTextChanged(int id, string phone);
    }
}
