﻿using PhoneBook.Entity.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Entity
{
    public class DetailContactEntity : IDetailContactEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string MainPhoneNumber { get; set; }
        public string Photo { get; set; }
        public Dictionary<int, string> AdditionalPhoneList { get; set; }

        public DetailContactEntity()
        {
        }

        public DetailContactEntity(string name, string surname, string phone, Dictionary<int,string> phoneList)
        {
            Name = name;
            Surname = surname;
            MainPhoneNumber = phone;
            AdditionalPhoneList = phoneList;
        }


        public override bool Equals(object obj)
        {
            var entity = obj as DetailContactEntity;
            return entity != null &&
                   Name == entity.Name &&
                   Surname == entity.Surname &&
                   MainPhoneNumber == entity.MainPhoneNumber &&
                   AdditionalPhoneList == entity.AdditionalPhoneList;
        }

        public override int GetHashCode()
        {
            var hashCode = 1387626102;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Surname);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(MainPhoneNumber);
            return hashCode;
        }
    }
}
