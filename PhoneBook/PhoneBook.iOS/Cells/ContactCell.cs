﻿using System;

using Foundation;
using UIKit;

namespace PhoneBook.iOS.Cells
{
    public partial class ContactCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("ContactCell");
        public static readonly UINib Nib;

        static ContactCell()
        {
            Nib = UINib.FromName("ContactCell", NSBundle.MainBundle);
        }

        protected ContactCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
    }
}
