﻿using PhoneBook.Validation.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Validation
{
    public class ValidationDetailContact : IValidationDetailContact
    {
        public bool CheckPhone(string phone)
        {
            if (string.IsNullOrEmpty(phone))
            {
                return true;
            }

            return Validation.CheckValue("^[+]?[0-9]{6,18}$", phone);
        }
    }
}
