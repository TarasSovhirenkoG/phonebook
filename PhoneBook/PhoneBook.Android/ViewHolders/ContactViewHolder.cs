﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace PhoneBook.Droid.ViewHolders
{
    class ContactViewHolder : RecyclerView.ViewHolder, View.IOnClickListener
    {
        public TextView TextViewName { get; set; }
        public TextView TextViewSurname { get; set; }
        public TextView TextViewNumber { get; set; }
        public ImageView ImageViewPhoto { get; set; }
        private IItemClickListener _itemClickListener { get; set; }

        public ContactViewHolder(View view) : base(view)
        {
            TextViewName = view.FindViewById<TextView>(Resource.Id.txtcontactname);
            TextViewSurname = view.FindViewById<TextView>(Resource.Id.surname);
            TextViewNumber = view.FindViewById<TextView>(Resource.Id.txtnumber);
            ImageViewPhoto = view.FindViewById<ImageView>(Resource.Id.imgView);
            view.SetOnClickListener(this);
        }

        public void SetItemClickListener(IItemClickListener itemClickListener)
        {
            this._itemClickListener = itemClickListener;
        }

        public void OnClick(View v)
        {
            _itemClickListener.OnClick(v, AdapterPosition);
        }
    }
}