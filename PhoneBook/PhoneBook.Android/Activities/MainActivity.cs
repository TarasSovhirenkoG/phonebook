﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using SQLite;
using System.IO;
using Android.Content.Res;
using PhoneBook.Droid.Fragments;

namespace PhoneBook.Droid.Activities
{
    [Activity(Label = "PhoneBook", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.ContactListActivity);

            var fragmentTransaction = FragmentManager.BeginTransaction();
            fragmentTransaction.Add(Resource.Id.contacts_fragment_container, new ContactListFragment());
            fragmentTransaction.Commit();

            /* SetContentView(Resource.Layout.PhoneBookList);
             _recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);

             var dbName = "db_contacts.sqlite";
             var dbPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments), dbName);

             DataProvider provider = new DataProvider(dbPath, dbName);
             provider.CreateDatabase(this);
             var dbContacts = provider.GetAllContacts();

             _contactItems = new List<Contact>();
             foreach (var contact in dbContacts)
             {
                 _contactItems.Add(contact);
             }

             _recyclerViewLayoutManger = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);
             _recyclerView.SetLayoutManager(_recyclerViewLayoutManger);
             _recyclerViewAdapter = new RecyclerAdapter(_contactItems, this);
             _recyclerView.SetAdapter(_recyclerViewAdapter);*/
        }
    }
}

