﻿using System;

using Android.Views;
using Android.Content;
using Android.Widget;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using PhoneBook.Entity.Interfaces;
using PhoneBook.Droid.ViewHolders;
using PhoneBook.Enumerations;

namespace PhoneBook.Droid.Adapters
{
    class PhoneRecyclerAdapter : RecyclerView.Adapter
    {
        public List<string> PhoneList { get; set; }
        private Context _context;

        /*public PhoneRecyclerAdapter(List<string> phones, Context context)
        {
            this._phones = phones;
            this._context = context;
            NotifyDataSetChanged();
        }*/

        public override int GetItemViewType(int position)
        {
            return 0;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.PhoneCell, parent, false);
            //view.LayoutParameters.Height = parent.Height / 5;
            return new PhoneViewHolder(view);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            if (holder is PhoneViewHolder)
            {
                ((PhoneViewHolder)holder).PhoneEditText.Text = PhoneList[position];
            }
        }

        public override int ItemCount
        {
            get
            {
                return PhoneList.Count;
            }
        }
    }

    public class PhoneViewHolder : RecyclerView.ViewHolder
    {
        public EditText PhoneEditText { get; set; }

        public PhoneViewHolder(View view) : base(view)
        {
            PhoneEditText = view.FindViewById<EditText>(Resource.Id.PhoneCell_phone_editText);
        }
    }
}